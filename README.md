# calServer-LabVIEW-Client

 is a sample project to use REST API of calServer Lab Management from National Instruments LabVIEW software:

  - GET | POST | PATCH | DELETE


# New Features!

  - get calibration date of an inventory with filter to asset number
  - add an inventory record
  - add an calibration record


# Contents

[TOC]


### Requirements

   - LabVIEW Community 2020;
   - VI Package Manager 2020;
   - Windows 10 or higher.

### using calServer API 

  - see [API-DOCUMENT.md](./API-DOCUMENT.md )

### 3rd party libs are needed:

   - JKI State Machine v2018.0.7.45 by JKI;
   - JKI HTTP REST Client v1.3.3.25 by JKI;
   - JSONtext v1.3.1.84 by JDP Science;
   - OpenG Application Control Library v4.1.0.7 by OpenG.org;
   - OpenG Array Library v4.1.1.14 by OpenG.org;
   - OpenG Error Library v4.2.0.23 by OpenG.org;
   - OpenG File Library v4.0.1.22 by OpenG.org;
   - OpenG LabVIEW Data Library v4.2.0.21 by LAVA;
   - OpenG String Library v4.1.0.12 by OpenG.org.

   Note: required packages could be found in "packages" directory of the repository.


### Installation


```sh
$ will follow
```


### Development

Want to contribute? Great!


### Todos

 - ...

License
----
All Rights Reserved by calHelp. Copyright calHelp© 2019. Our application is 100% open-source and ready to customize from back-end to front-end You are fully allowed to extend, customize or redesign only for your licensed business only.
