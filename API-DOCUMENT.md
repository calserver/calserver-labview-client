# CALHELP API DOCUMENTATION

## - Inventory

### Get Data (Method GET)

**Javascript Syntax**

```sh
$.ajax({
    type: "GET",
    dataType: 'json',
    url: "http://domain.com/api/inventory",
    data: {
        HTTP_X_REST_USERNAME: 'username', 
        HTTP_X_REST_PASSWORD: 'password', 
        HTTP_X_REST_API_KEY: 'api_key',
        filter: [{"property": "I4202","value": "FLUKE","operator": "="}]
    },
    success:function(data) {
      console.log(data);
    },
    error:function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    } 
  }); 
```

**Command Line Syntax**

```sh
curl -i -H "Accept: application/json" -H "X_REST_USERNAME: admin" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\ 
http://domain.com/api/inventory
```

**Response**

```sh
{
    "success": true,
    "message": "Record(s) Found",
    "data": {
        "totalCount": 1358,
        "inventory": [
            {
                "I4201": "002774",
                "I4202": "EBRO",
                "I4203": "EBI-125A-EM-150",
                "I4204": "TEMPERATUR DATENLOGGER",
                "I4205": "I",
                "I4206": "b",
                "I4207": null,
                "I4208": null,
                "I4209": "3",
                "I4210": null,
                "I4211": "READY TO USE",
                "I4212": null,
                "I4213": null,
                "I4214": null,
                "I4215": null,
                "I4216": null,
                "I4217": null,
                "I4218": null,
                "I4219": null,
                "I4220": null,
                "I4221": null,
                "I4222": null,
                "I4223": "EBRO",
                "I4224": "2014-07-16 00:00:00",
                "I4225": "A",
                "I4226": "Y",
                "I4228": "M",
                "I4229": "12",
                "I4230": "0",
                "I4231": "TEMPERATURMESSSYSTEME",
                "I4232": "test1",
                "I4233": null,
                "I4234": "2014-10-22",
                "I4235": null,
                "I4236": "2007-02-13 13:51:37",
                "I4237": "hg",
                "I4238": "2014-03-25 23:08:59",
                "I4239": "mt",
                "I4240": "MT",
                "I4299": "A",
                "MTAG": "00277:1171374697",
                "I4241": null,
                "I4242": "REGION NORD",
                "I4243": "SCHACHT",
                "idsrc": "MT7",
                "IPUBLISH": "mtrack",
                "ktag": "239:1171370203",
                "replic": null,
                "I4258": "N",
                "I4244": "TEMPERATUR UND FEUCHTE",
                "I4245": null,
                "I4246": null,
                "I4247": null,
                "I4248": null,
                "I4249": null,
                "I4250": null,
                "I4251": null,
                "I4252": null,
                "I4253": null,
                "I4254": null,
                "I4255": null,
                "I4256": "-40.00",
                "I4257": "125.00",
                "I4259": "0",
                "I4260": null,
                "I4261": null,
                "I4262": null
            },
            {...},
            {...},
        ]
    }
}
```

## Create New Inventory (Method POST)

**Javascript Syntax** :   

Get rules and column name information at:
http://domain.com/api/inventory/rules

```sh
var sampleData = {
      I4203:"002774", 
      I4201:"0033127743", 
      I4202: "EBRO", 
      MTAG:"133123", 
      I4206:"a",
      I4224: "2015-05-05", 
      ktag: "006:984729367",
      I4228: "D",
      I4229: "32",
      HTTP_X_REST_USERNAME: 'username', 
      HTTP_X_REST_PASSWORD: 'password',
      HTTP_X_REST_API_KEY: 'api_key'
};
    
$.ajax({        
    type: "POST",
    dataType: 'json',
    url: "http://domain.com/api/inventory",
    data: sampleData,
    success:function(data) {
      console.log(data);
    },
    error:function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    } 
  }); 
```

**Command Line Syntax**

```sh
curl -l -H "Accept: application/json" -H "X_REST_USERNAME: username" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
-X POST -d '{I4203:"002774", I4201:"0033127332743", I4202: "EBRO", MTAG:"133123", I4206:"a", I4224: "2015-05-05", ktag: "006:984729367", I4228: "D", I4229: "32", }' http://domain.com/api/inventory
```

**Response**

```sh
=========Fail=========
{
    "success": false,
    "message": "{\"I4201\":[\"This asset number is already in use. Please choose a different one\"],\"I4228\":[\"Interval Units cannot be blank.\",\"Interval Units cannot be blank.\"],\"I4229\":[\"Recall Interval cannot be blank.\"],\"I4206\":[\"221 is not allowed for I4206, Please select from list (a,b,c)\"]}",
    "data": {
        "errorCode": "400",
        "message": "{\"I4201\":[\"This asset number is already in use. Please choose a different one\"],\"I4228\":[\"Interval Units cannot be blank.\",\"Interval Units cannot be blank.\"],\"I4229\":[\"Recall Interval cannot be blank.\"],\"I4206\":[\"221 is not allowed for I4206, Please select from list (a,b,c)\"]}"
    }
}

=========Success=========
{
    "success": true,
    "message": "Record Created",
    "data": {
        "totalCount": 1,
        "inventory": {
            "I4201": "0033127332743",
            "I4202": "EBRO",
            "I4203": "002774",
            "I4204": null,
            "I4205": null,
            "I4206": "a",
            "I4207": null,
            "I4208": null,
            "I4209": "1",
            "I4210": null,
            "I4211": null,
            "I4212": null,
            "I4213": null,
            "I4214": null,
            "I4215": null,
            "I4216": null,
            "I4217": null,
            "I4218": null,
            "I4219": null,
            "I4220": null,
            "I4221": null,
            "I4222": null,
            "I4223": null,
            "I4224": "2015-05-05 00:00:00",
            "I4225": "A",
            "I4226": "Y",
            "I4228": "D",
            "I4229": "32",
            "I4230": "0",
            "I4231": null,
            "I4232": null,
            "I4233": null,
            "I4234": null,
            "I4235": null,
            "I4236": null,
            "I4237": null,
            "I4238": null,
            "I4239": null,
            "I4240": "MT",
            "I4299": "A",
            "MTAG": "e941ec89-f9bd-0303-c10c-a46118f5186f",
            "I4241": null,
            "I4242": null,
            "I4243": null,
            "idsrc": null,
            "IPUBLISH": "mtrack",
            "ktag": "006:984729367",
            "replic": null,
            "I4258": "N",
            "I4244": null,
            "I4245": null,
            "I4246": null,
            "I4247": null,
            "I4248": null,
            "I4249": null,
            "I4250": null,
            "I4251": null,
            "I4252": null,
            "I4253": null,
            "I4254": null,
            "I4255": null,
            "I4256": null,
            "I4257": null,
            "I4259": "0",
            "I4260": null,
            "I4261": null,
            "I4262": "'"
        }
    }
}
```

## Update Inventory (Method PUT)

**Javascript Syntax** :

+ Get all inventories : <domain.com>/api/inventory (see GET DATA above)

+ Get rules and column name information at:

http://domain.com/api/inventory/rules

```sh
var sampleData = {
      I4203:"002774", 
      I4201:"0033127743", 
      I4202: "EBRO", 
      MTAG:"133123", 
      I4206:"a",
      I4224: "2015-05-05", 
      ktag: "006:984729367",
      I4228: "D",
      I4229: "32",
      HTTP_X_REST_USERNAME: 'username', 
      HTTP_X_REST_PASSWORD: 'password',
      HTTP_X_REST_API_KEY: 'api_key'
};
        
$.ajax({        
    type: "PUT",
    dataType: 'json',
    url: "http://domain.com/api/inventory/MTAG",
    data: sampleData,
    success:function(data) {
      console.log(data);
    },
    error:function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    } 
  }); 
```

**Command Line Syntax**

```sh
curl -l -H "Accept: application/json" -H "X_REST_USERNAME: username" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
-X POST -d '{I4203:"002774", I4201:"0033127332743", I4202: "EBRO", MTAG:"133123", I4206:"a", I4224: "2015-05-05", ktag: "006:984729367", I4228: "D", I4229: "32", }' http://domain.com/api/inventory
```

**Response**

```sh
=========Fail=========
{
    "success": false,
    "message": "RESOURCE '00277_1171374697' NOT FOUND",
    "data": {
        "errorCode": 404,
        "message": "RESOURCE '00277_1171374697' NOT FOUND"
    }
}

=========Success=========
{
    "success": true,
    "message": "Record Updated",
    "data": {
        "totalCount": 1,
        "inventory": {
            "I4201": "0033127sswwasw743",
            "I4202": "EBRO",
            "I4203": "002774",
            "I4204": "TEMPERATUR DATENLOGGER",
            "I4205": "I",
            "I4206": "a",
            "I4207": null,
            "I4208": null,
            "I4209": "3",
            "I4210": null,
            "I4211": "READY TO USE",
            "I4212": null,
            "I4213": null,
            "I4214": null,
            "I4215": null,
            "I4216": null,
            "I4217": null,
            "I4218": null,
            "I4219": null,
            "I4220": null,
            "I4221": null,
            "I4222": null,
            "I4223": "EBRO",
            "I4224": "2015-05-05 00:00:00",
            "I4225": "A",
            "I4226": "Y",
            "I4228": "D",
            "I4229": "32",
            "I4230": "0",
            "I4231": "TEMPERATURMESSSYSTEME",
            "I4232": "test1",
            "I4233": null,
            "I4234": "2014-10-22",
            "I4235": null,
            "I4236": "2007-02-13 13:51:37",
            "I4237": "hg",
            "I4238": "2014-03-25 23:08:59",
            "I4239": "mt",
            "I4240": "MT",
            "I4299": "A",
            "MTAG": "133123",
            "I4241": null,
            "I4242": "REGION NORD",
            "I4243": "SCHACHT",
            "idsrc": "MT7",
            "IPUBLISH": "mtrack",
            "ktag": "006:984729367",
            "replic": null,
            "I4258": "N",
            "I4244": "TEMPERATUR UND FEUCHTE",
            "I4245": null,
            "I4246": null,
            "I4247": null,
            "I4248": null,
            "I4249": null,
            "I4250": null,
            "I4251": null,
            "I4252": null,
            "I4253": null,
            "I4254": null,
            "I4255": null,
            "I4256": "-40.00",
            "I4257": "125.00",
            "I4259": "0",
            "I4260": null,
            "I4261": null,
            "I4262": null
        }
    }
}
```

## - Calibration

### Get Data (Method GET)

**Javascript Syntax**

```sh
$.ajax({
    url:'http://domain.com/api/calibration',
    type: "GET",
    dataType: 'json',
    data: {
        HTTP_X_REST_USERNAME: 'username',
        HTTP_X_REST_PASSWORD: 'password', 
        HTTP_X_REST_API_KEY: 'api_key', 
        filter: [{"property": "inventory.I4201","value": "dummy","operator": "="}]
    },
    success: function(data) {
      console.log(data);
    },
    error: function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    }
  });
```

**Command Line Syntax**

```sh
curl -i -H "Accept: application/json" -H "X_REST_USERNAME: admin" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
http://domain.com/api/calibration
```

**Response**

```sh
{
    "success": true,
    "message": "Record(s) Found",
    "data": {
        "totalCount": 7,
        "calibration": [
        {
            "CTAG": "5095:1250152868",
            "C2301": "2009-08-13",
            "C2302": "12",
            "C2303": "2010-08-13",
            "C2304": null,
            "C2306": null,
            "C2307": "Gutknecht",
            "C2309": "0.00",
            "C2310": "0.0",
            "C2311": "23.70",
            "C2312": "46.00",
            "C2313": "Thermo-KL",
            "C2314": "100000722",
            "C2316": null,
            "C2320": "THERMOELEMENT 37..400 ATC 650 - 600..1000�C ROF V7",
            "C2321": "1.00",
            "C2322": "Y",
            "C2323": "Y",
            "C2324": null,
            "C2326": "",
            "C2327": "",
            "C2328": null,
            "C2329": "",
            "C2330": "10",
            "C2331": "M",
            "C2332": "2",
            "C2333": "08:41:08",
            "C2334": "08:41:08",
            "C2335": "2009-08-13 08:43:53",
            "C2336": "hg",
            "C2337": "2014-03-27 22:30:45",
            "C2338": "mt",
            "C2339": "0",
            "C2341": "knapp auerhalb Klasse 2 bei 600C",
            "MTAG": "5095:945766016",
            "C2362": "9",
            "C2308": "FOUND-LEFT",
            "C2315": "4.0",
            "C2342": "2009-08-13 00:00:00",
            "C2343": "08:43:05",
            "C2317": "100.0",
            "C2318": "Nominal",
            "C2319": "y",
            "C2325": "n",
            "C2350": "0",
            "C2351": "1",
            "C2352": "1",
            "C2353": "0",
            "C2354": "1428",
            "C2355": "WK-T",
            "C2356": "0908",
            "C2357": "1,5",
            "C2358": "",
            "C2359": "Werkskalibrierung TC - Temperatur - Id-Nr. 50082714",
            "C2360": "",
            "C2361": "",
            "C2363": "n",
            "cdsrc": "RUN",
            "CPUBLISH": null,
            "C2381": "",
            "C2382": "N",
            "C2364": "",
            "C2365": "",
            "C2366": "Langenselbold",
            "C2367": "Kalibrierlabor, EG Sed",
            "C2368": "2",
            "C2369": "",
            "C2370": "",
            "C2371": null,
            "C2372": null,
            "C2373": null,
            "C2374": null,
            "C2375": "0.00",
            "C2376": "0.00",
            "C2377": "0.00",
            "C2378": "0.00",
            "C2379": "0.00",
            "C2380": null,
            "C2383": "0",
            "C2344": null,
            "C2345": null,
            "C2346": null,
            "C2347": null,
            "C2384": null,
            "C2385": "",
            "C2386": null,
            "C2387": null,
            "C2388": null,
            "C2389": null,
            "C2390": null,
            "C2391": null,
            "C2392": null,
            "C2393": "",
            "C2394": "",
            "C2395": "",
            "C2396": "5095_090813",
            "combine": ""
            },
            {...},
            {...},
        ]
    }
}
```

## Create New Calibration (Method POST)

**Javascript Syntax** :

POST http://domain.com/api/calibration/insert

```sh
var sampleData = {
      MTAG: "00f4373d9a14a34590d7c13e2f478737",
      C2301: "2014-03-05",
      C2333: "12:30:00",
      C2303: "2017-02-15",
      C2307: "Goschier",
      HTTP_X_REST_USERNAME: 'username',
      HTTP_X_REST_PASSWORD: 'password',
      HTTP_X_REST_API_KEY: 'api_key'
};
        
$.ajax({
    url: 'http://domain.com/api/calibration/insert',
    type: "POST",
    dataType: 'json',
    data: sampleData,
    success: function(data) {
      console.log(data);
    },
    error: function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    }
  });
```

**Command Line Syntax**

```sh
curl -l -H "Accept: application/json" -H "X_REST_USERNAME: username" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
-X POST -d '{MTAG:"00f4373d9a14a34590d7c13e2f478737", C2301:"2014-03-05", C2333: "12:30:00", C2303:"2017-02-15", C2307:"Goschier"}' http://domain.com/api/calibration/insert
```

**Response**

```sh
=========Fail=========
{
    "success": false,
    "errors": {
        "C2323": "Pass cannot be blank.",
        ...
    }
}

=========Success=========
{
    "success": true,
    "message": "Record Created"
}
```

## Update Calibration (Method POST)

**Javascript Syntax** :

+ Get all calibrations : <domain.com>/api/calibration (see GET DATA above)

+ Update calibration at:
http://domain.com/api/calibration/update

```sh
var sampleData = {
    CTAG: "0106a33e0a336342913fd5b4e4230e6e",
    MTAG: "00f4373d9a14a34590d7c13e2f478737",
    C2301: "2014-03-07",
    C2333: "12:30:00",
    C2303: "2017-02-25",
    C2307: "Goschier",
    HTTP_X_REST_USERNAME: 'username',
    HTTP_X_REST_PASSWORD: 'password',
    HTTP_X_REST_API_KEY: 'api_key'
};
    
$.ajax({    
    url: 'http://domain.com/api/calibration/update',
    type: "POST",
    dataType: 'json',
    data: sampleData,
    success: function(data) {
      console.log(data);
    },
    error: function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    }
  });
```

**Command Line Syntax**

```sh
curl -l -H "Accept: application/json" -H "X_REST_USERNAME: username" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
-X POST -d '{CTAG: "0106a33e0a336342913fd5b4e4230e6e", MTAG:"00f4373d9a14a34590d7c13e2f478737", C2301:"2014-03-07", C2333: "12:30:00", C2303:"2017-02-25", C2307:"Goschier"}' http://domain.com/api/calibration/update
```

**Response**

```sh
=========Fail=========
{
    "success": false,
    "errors": {
        "C2323": "Pass cannot be blank.",
        ...
    }
}

=========Success=========
{
    "success": true,
    "message": "Record Updated"
}
```

## - Customer

### Get Data (Method GET)

**Javascript Syntax**

```sh
$.ajax({
    url: 'http://domain.com/api/customer',
    type: "GET",
    dataType: 'json',
    data: {
        HTTP_X_REST_USERNAME: 'username',
        HTTP_X_REST_PASSWORD: 'password',
        HTTP_X_REST_API_KEY: 'api_key', 
        filter: [{"property":"K4602","value":"Ammicht","operator": "="}]
    },
    success: function(data) {
      console.log(data);
    },
    error: function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    }
  });
```

**Command Line Syntax**

```sh
curl -i -H "Accept: application/json" -H "X_REST_USERNAME: admin" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
http://domain.com/api/customer
```

**Response**

```sh
{
    "success": true,
    "message": "Record(s) Found",
    "data": {
        "totalCount": 1,
        "customer": [
            {
                "KTAG": "058:1010507906",
                "KPUBLISH": "mtrack",
                "K4601": "058",
                "K4602": "Ammicht",
                "K4603": "Hundscker 28",
                "K4604": "Haibach",
                "K4605": "D",
                "K4606": "63808",
                "K4607": "Hundscker 28",
                "K4608": "Haibach",
                "K4609": "D",
                "K4610": "63808",
                "K4611": null,
                "K4612": "Herrn",
                "K4613": "Ernst",
                "K4614": "Thermo Electron LED GmbH",
                "K4615": "Region Sed",
                "K4616": "Servicetechniker",
                "K4617": null,
                "K4618": null,
                "K4619": null,
                "K4620": null,
                "K4621": null,
                "K4622": null,
                "K4623": null,
                "K4624": null,
                "K4625": null,
                "K4626": null,
                "K4627": null,
                "K4628": null,
                "K4640": "MT",
                "K4629": "2002-01-08 00:00:00",
                "K4630": "mg",
                "K4631": "2009-06-09 22:52:45",
                "K4632": "mt",
                "KDSRC": null,
                "kreplic": null,
                "K4633": null,
                "K4634": null,
                "K4635": null,
                "K4636": null,
                "K4637": null
            }
        ]
    }
}
```

## Create New Customer (Method POST)

**Javascript Syntax** :

POST http://domain.com/api/customer/insert

```sh
var sampleData = {
      K4601: "20",
      K4602: "Mary Pooschke",
      K4603: "Schafbaumweg 67",
      K4604: "Wimsheim",
      K4605: "US",
      K4606: "34567",
      K4608: "Haibach",
      HTTP_X_REST_USERNAME: 'username',
      HTTP_X_REST_PASSWORD: 'password',
      HTTP_X_REST_API_KEY: 'api_key'
};
        
$.ajax({    
    url: 'http://domain.com/api/customer/insert',
    type: "POST",
    dataType: 'json',
    data: sampleData,
    success: function(data) {
      console.log(data);
    },
    error: function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    }
  });
```

**Command Line Syntax**

```sh
curl -l -H "Accept: application/json" -H "X_REST_USERNAME: username" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
-X POST -d '{K4601: "20", K4602: "Mary Pooschke", K4603:"Schafbaumweg 67", K4604:"Wimsheim", K4605: "US", K4606: "34567", K4608: "Haibach"}' http://domain.com/api/customer/insert
```

**Response**

```sh
=========Fail=========
{
    "success": false,
    "errors": {
        "K4601": "Customer ID cannot be blank.",
        ...
    }
}

=========Success=========
{
    "success": true,
    "message": "Record Created"
}
```

## Update Customer (Method POST )

**Javascript Syntax** :

+ Get all customers : <domain.com>/api/customer (see GET DATA above)

+ Update customer at:
http://domain.com/api/customer/update

```sh
var sampleData = {
    KTAG: "558c32a2-d961-6f2f-d15a-0fe53710e785",
    K4601: "20",
    K4602: "Mary Pooschke",
    K4603: "Schafbaumweg 67",
    K4604: "Wimsheim",
    K4605: "US",
    K4606: "34567",
    K4608: "Haibach",
    HTTP_X_REST_USERNAME: 'username',
    HTTP_X_REST_PASSWORD: 'password',
    HTTP_X_REST_API_KEY: 'api_key'
};
    
$.ajax({    
    url: 'http://domain.com/api/customer/update',
    type: "POST",
    dataType: 'json',
    data: sampleData,
    success: function(data) {
      console.log(data);
    },
    error: function (xhr, ajaxOptions, thrownError){
      console.log(xhr.responseText);
    }
  });
```

**Command Line Syntax**

```sh
curl -l -H "Accept: application/json" -H "X_REST_USERNAME: username" -H "X_REST_PASSWORD: password" -H "X_REST_API_KEY: api_key"\
-X POST -d '{KTAG: "558c32a2-d961-6f2f-d15a-0fe53710e785", K4601: "20", K4602: "Mary Pooschke", K4603:"Schafbaumweg 67", K4604:"Wimsheim", K4605: "US", K4606: "34567", K4608: "Haibach"}' http://domain.com/api/customer/update
```

**Response**

```sh
=========Fail=========
{
    "success": false,
    "errors": {
        "K4601": "Customer ID cannot be blank.",
        ...
    }
}

=========Success=========
{
    "success": true,
    "message": "Record Updated"
}
```